//
//  WSGithub.m
//  DesafioConcrete
//
//  Created by Renato Mori on 19/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import "WSGithub.h"

@implementation WSGithub

- (instancetype)initWithPage:(int)page
{
    self = [super initWithUrl:@"https://api.github.com/search/repositories"];
    if(self){
        self.q = @"language:Java";
        self.sort = @"stars";
        self.page = [NSString stringWithFormat: @"%d",page];
    }
    return self;
}

-(RetornoGithub*)getRetorno{
    if(self.isCompleted){
        return [[RetornoGithub alloc]initWithJSON:self.retornoHTML];
    }else{
        return nil;
    }
}

@end
