//
//  WSGithub.h
//  DesafioConcrete
//
//  Created by Renato Mori on 19/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebService.h"
#import "RetornoGithub.h"

@interface WSGithub : WebService

@property (nonatomic,strong) NSString * q;
@property (nonatomic,strong) NSString * sort;
@property (nonatomic,strong) NSString * page;

-(id) initWithPage:(int)page;
-(RetornoGithub*)getRetorno;


@end
