//
//  WSPullRequest.m
//  DesafioConcrete
//
//  Created by Renato Mori on 19/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import "WSPullRequest.h"

@implementation WSPullRequest


- (instancetype)initWithUrl:(NSString *)url
{
    self = [super init];
    if (self) {
        self.url = url;
    }
    return self;
}

-(RetornoPullRequest*)getRetorno{
    if(self.isCompleted){
        return [[RetornoPullRequest alloc]initWithJSON:self.retornoHTML];
    }else{
        return nil;
    }
}

@end
