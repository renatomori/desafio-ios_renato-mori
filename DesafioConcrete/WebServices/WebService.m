//
//  WebService.m
//  DesafioConcrete
//
//  Created by Renato Mori on 19/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import "WebService.h"



@implementation WebService
static NSObject * lockPost;

- (instancetype)initWithUrl:(NSString *)url
{
    self = [super init];
    if (self) {
        if(lockPost == nil){
            lockPost = [[NSObject alloc]init];
        }
        self.isCompleted = NO;
        self.url = url;
        self.receivedData = [NSMutableData dataWithCapacity:0];
    }
    return self;
}

-(NSString*)getPost{
    NSString * post = @"";
    
    unsigned int varCount;
    
    Ivar *vars = class_copyIvarList(self.class, &varCount);
    
    for (int i = 0; i < varCount; i++) {
        Ivar var = vars[i];
        
        //        const char* name = ivar_getName(var);
        //        const char* typeEncoding = ivar_getTypeEncoding(var);
        NSString * name = [[NSString stringWithUTF8String:ivar_getName(var)] substringFromIndex:1];
        
        
        if(class_getProperty(self.class, [name UTF8String])){
            NSString *value = [self valueForKey:[NSString stringWithUTF8String:[name UTF8String]]];
            post = [post stringByAppendingString:[NSString stringWithFormat: @"&%@=%@",name, value]];
        }
    }
    
    free(vars);
    if([post isEqualToString:@""]){
        return @"";
    }
    return [post substringFromIndex:1];
}

-(BOOL)postar{
    @synchronized (lockPost) {
        self.post = [self getPost];
        NSData *postData = [self.post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
       // NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:[[self.url stringByAppendingString:@"?"] stringByAppendingString:self.post]]];
        [request setHTTPMethod:@"GET"];
        //[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        //[request setHTTPBody:postData];
        
        NSHTTPURLResponse *response = nil;
        NSError *error = nil;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        NSLog(@"url=%@",self.url);
        NSLog(@"post=%@",self.post);
        
        if(responseData) {
            NSLog(@"Connection Successful");
            self.retornoHTML = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
            NSLog(@"retorno=%@",self.retornoHTML);
            self.isCompleted = YES;
            return YES;
        } else {
            NSLog(@"Connection could not be made");
            NSLog(@"%@",error);
            return  NO;
        }
    }
    
}
@end
