//
//  WebService.h
//  DesafioConcrete
//
//  Created by Renato Mori on 19/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

#define mustOverride() @throw [NSException exceptionWithName:NSInvalidArgumentException reason:[NSString stringWithFormat:@"%s must be overridden in a subclass/category", __PRETTY_FUNCTION__] userInfo:nil]
#define methodNotImplemented() mustOverride()

@interface WebService : NSObject


@property (nonatomic,strong) NSString * url;
@property (nonatomic,strong) NSString * retornoHTML;
@property (nonatomic,strong) NSString * post;
@property (nonatomic) BOOL isCompleted;

@property (nonatomic,strong) NSMutableData * receivedData;


-(instancetype)initWithUrl:(NSString *)url;
-(BOOL)postar;

@end
