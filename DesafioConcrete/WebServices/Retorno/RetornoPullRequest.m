//
//  RetornoPullRequest.m
//  DesafioConcrete
//
//  Created by Renato Mori on 20/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import "RetornoPullRequest.h"

@implementation RetornoPullRequest

- (id)initWithJSON:(NSString *)json
{
    self = [super init];
    if (self) {
        [self setWithJSON:json];
        self.index = -1;
    }
    return self;
}

-(BOOL) next{
    self.index++;
    return self.index < self.items.count;
}


//Nome / Foto do autor do PR,   //
-(NSString *) nome{
    NSString* temp = [[[super.items objectAtIndex:self.index]
                       objectForKey:@"user"]
                      objectForKey:@"login"];
    
    if(temp == nil || [temp isKindOfClass:[NSNull class]]){
        return @"";
    }
    return temp;
}
-(NSString *) fotoUrl{
    NSString* temp = [[[super.items objectAtIndex:self.index]
                       objectForKey:@"user"]
                      objectForKey:@"avatar_url"];
    
    if(temp == nil || [temp isKindOfClass:[NSNull class]]){
        return @"";
    }
    return temp;
}
//Título do PR,                 //
-(NSString *) titulo{
    NSString* temp = [[super.items objectAtIndex:self.index]
                      objectForKey:@"title"];
    
    if(temp == nil || [temp isKindOfClass:[NSNull class]]){
        return @"";
    }
    return temp;
}
//Data do PR e                  //
-(NSString *) data{
    NSString* temp = [[super.items objectAtIndex:self.index]
                      objectForKey:@"created_at"];
    if(temp == nil || [temp isKindOfClass:[NSNull class]]){
        return @"";
    }
    return temp;
}
//Body do PR                    //
-(NSString *) body{
    NSString* temp = [[super.items objectAtIndex:self.index]
                      objectForKey:@"body"];
    if(temp == nil || [temp isKindOfClass:[NSNull class]]){
        return @"";
    }
    return temp;
}


-(NSString *)html_url{
    NSString* temp = [[super.items objectAtIndex:self.index]
                      objectForKey:@"html_url"];
    if(temp == nil || [temp isKindOfClass:[NSNull class]]){
        return @"";
    }
    return temp;
}


@end
