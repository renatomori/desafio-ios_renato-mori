//
//  RetornoGithub.m
//  DesafioConcrete
//
//  Created by Renato Mori on 19/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import "RetornoGithub.h"

@implementation RetornoGithub

- (id)initWithJSON:(NSString *)json
{
    self = [super init];
    if (self) {
        [self setWithJSON:json];
        self.index = -1;
    }
    return self;
}

-(BOOL) next{
    self.index++;
    return self.index < self.items.count;
}
//Nome do repositório,// name
-(NSString *) name{
    NSString* temp = [[super.items objectAtIndex:self.index] objectForKey:@"name"];
    if(temp == nil || [temp isKindOfClass:[NSNull class]]){
        return @"";
    }
    return temp;
}
//Descrição do Repositório,// description
-(NSString *) description{
    NSString* temp = [[super.items objectAtIndex:self.index] objectForKey:@"description"];
    if(temp == nil || [temp isKindOfClass:[NSNull class]]){
        return @"";
    }
    return temp;
}

//Nome // owner -> login
-(NSString *) login{
    NSString* temp = [[[super.items objectAtIndex:self.index]
            objectForKey:@"owner"]
            objectForKey:@"login"];
    if(temp == nil || [temp isKindOfClass:[NSNull class]]){
        return @"";
    }
    return temp;
}
// Foto do autor // owner -> avatar_url
-(NSString *) avatar_url{
    NSString* temp = [[[super.items objectAtIndex:self.index]
                                     objectForKey:@"owner"]
                                     objectForKey:@"avatar_url"];
    if(temp == nil || [temp isKindOfClass:[NSNull class]]){
        return @"";
    }
    return temp;
}

//Número de Stars,// stargazers_count
-(long) stargazers_count{
    id temp =[[super.items objectAtIndex:self.index] objectForKey:@"stargazers_count"];
    if(temp == nil || [temp isKindOfClass:[NSNull class]]){
        return 0;
    }
    return [temp intValue];
}
//Número de Forks// forks
-(long) forks{
    id temp =[[super.items objectAtIndex:self.index] objectForKey:@"forks"];
    if(temp == nil || [temp isKindOfClass:[NSNull class]]){
        return 0;
    }
    return [temp intValue];
}

-(NSString *) pulls_url{
    NSString* temp = [[super.items objectAtIndex:self.index] objectForKey:@"pulls_url"];
    if(temp == nil || [temp isKindOfClass:[NSNull class]]){
        return @"";
    }
    temp = [temp stringByReplacingOccurrencesOfString:@"{/number}" withString:@""];
    return temp;
}

@end
