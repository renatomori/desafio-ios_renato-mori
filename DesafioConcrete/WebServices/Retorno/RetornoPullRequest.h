//
//  RetornoPullRequest.h
//  DesafioConcrete
//
//  Created by Renato Mori on 20/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import "Retorno.h"

@interface RetornoPullRequest : Retorno
@property (nonatomic) int index;

-(id)initWithJSON:(NSString *)json;

-(BOOL) next;

//Nome / Foto do autor do PR,   //
-(NSString *) nome;
-(NSString *) fotoUrl;
//Título do PR,                 //
-(NSString *) titulo;
//Data do PR e                  //
-(NSString *) data;
//Body do PR                    //
-(NSString *) body;

-(NSString *)html_url;
@end
