//
//  RetornoGithub.h
//  DesafioConcrete
//
//  Created by Renato Mori on 19/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Retorno.h"

@interface RetornoGithub : Retorno
@property (nonatomic) int index;

-(id)initWithJSON:(NSString *)json;

-(BOOL) next;

//Nome do repositório,// name
-(NSString *) name;
//Descrição do Repositório,// description
-(NSString *) description;

//Nome // owner -> login
-(NSString *) login;
// Foto do autor // owner -> avatar_url
-(NSString *) avatar_url;

//Número de Stars,// stargazers_count
-(long) stargazers_count;
//Número de Forks// forks
-(long) forks;



-(NSString *) pulls_url;

@end
