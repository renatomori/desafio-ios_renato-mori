//
//  Retorno.m
//  DesafioConcrete
//
//  Created by Renato Mori on 19/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import "Retorno.h"

@implementation Retorno

- (id)initWithJSON:(NSString*)json
{
    self = [super init];
    if (self) {
        [self setWithJSON:json];
    }
    return self;
}


- (void)setWithJSON:(NSDictionary *)json_obj vars:(Ivar *)vars varCount:(unsigned int)varCount
{
    if([json_obj isKindOfClass:[NSArray class]]){
        self.items = (NSArray *)json_obj;
    }else if(![json_obj isEqual:[NSNull null]]){
        for (int i = 0; i < varCount; i++) {
            Ivar var = vars[i];
            
            
            //        const char* name = ivar_getName(var);
            //        const char* typeEncoding = ivar_getTypeEncoding(var);
            NSString * name = [[NSString stringWithUTF8String:ivar_getName(var)] substringFromIndex:1];
            id value =[json_obj objectForKey:name];
            
            
            if(value){
                if(class_getProperty(self.class, [name UTF8String])){
                    [self setValue:value forKey:[NSString stringWithUTF8String:[name UTF8String]]];
                }
            }
        }
    }
}

-(void)setWithJSON:(NSString*)json
{
    NSError *jsonError;
    NSDictionary *json_obj = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding]
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
    
    unsigned int varCount;
    
    
    
    Ivar *vars = class_copyIvarList([self superclass], &varCount);
    [self setWithJSON:json_obj vars:vars varCount:varCount];
    free(vars);
    
    //vars = class_copyIvarList([self class], &varCount);
   // [self setWithJSON:[json_obj objectForKey:@"items"] vars:vars varCount:varCount];
   // free(vars);
    
}
@end
