//
//  Retorno.h
//  DesafioConcrete
//
//  Created by Renato Mori on 19/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface Retorno : NSObject

@property (nonatomic) long total_count;
@property (nonatomic) BOOL incomplete_results;
@property (nonatomic,strong) NSArray * items;

- (id)initWithJSON:(NSString*)json;

-(void)setWithJSON:(NSString*)json;
@end
