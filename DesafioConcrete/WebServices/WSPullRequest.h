//
//  WSPullRequest.h
//  DesafioConcrete
//
//  Created by Renato Mori on 19/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebService.h"
#import "RetornoPullRequest.h"

@interface WSPullRequest : WebService

- (instancetype)initWithUrl:(NSString *)url;
- (RetornoPullRequest*) getRetorno;


@end
