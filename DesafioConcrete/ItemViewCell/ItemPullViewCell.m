//
//  ItemPullViewCell.m
//  DesafioConcrete
//
//  Created by Renato Mori on 20/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import "ItemPullViewCell.h"

@implementation ItemPullViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onClick:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[self html_url]]];
}
@end
