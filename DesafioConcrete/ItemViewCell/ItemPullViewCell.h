//
//  ItemPullViewCell.h
//  DesafioConcrete
//
//  Created by Renato Mori on 20/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemPullViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel * lblNome;
@property (nonatomic,strong) IBOutlet UIImageView * imgFotoUrl;

@property (nonatomic,strong) IBOutlet UILabel * lblTitulo;
@property (nonatomic,strong) IBOutlet UILabel * lblData;
@property (nonatomic,strong) IBOutlet UILabel * lblBody;

@property (nonatomic,strong) NSString * html_url;
- (IBAction)onClick:(id)sender;

@end
