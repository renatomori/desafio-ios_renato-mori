//
//  ItemViewCell.m
//  DesafioConcrete
//
//  Created by Renato Mori on 18/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import "ItemViewCell.h"
#import "TrocaTela.h"

@implementation ItemViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)onClick:(id)sender {
    
    NSLog(@"%@",self.url);
    [TrocaTela setData:self.url];
    [TrocaTela trocarFrom:self.viewController To:@"pullTableViewController"];
    
}

@end
