//
//  AppDelegate.h
//  DesafioConcrete
//
//  Created by Renato Mori on 18/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (weak, nonatomic) UIViewController * current;
@property (weak, nonatomic) UIViewController * back;
@property (weak, nonatomic) id data;

@end

