//
//  TrocaTela.h
//  DesafioConcrete
//
//  Created by Renato Mori on 20/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TrocaTela : NSObject
+(void) trocarFrom:(UIViewController *)from To:(NSString *) target;
+(void) trocarFrom:(UIViewController *)from ToViewController:(UIViewController *) target;

+(void) back;

+(void) setData:(id) data;
+(id) getData;

@end
