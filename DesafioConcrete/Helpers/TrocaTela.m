//
//  TrocaTela.m
//  DesafioConcrete
//
//  Created by Renato Mori on 20/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import "TrocaTela.h"
#import "AppDelegate.h"

@implementation TrocaTela


+(void) trocarFrom:(UIViewController *)from To:(NSString *) target{
    UIViewController * viewcontroller = [from.storyboard instantiateViewControllerWithIdentifier:target];
    [from presentViewController:viewcontroller animated:YES completion:nil];
    ((AppDelegate*)[UIApplication sharedApplication].delegate).current = viewcontroller;
}

+(void)trocarFrom:(UIViewController *)from ToViewController:(UIViewController *) target{
    [from presentViewController:target animated:YES completion:nil];
    ((AppDelegate*)[UIApplication sharedApplication].delegate).current = target;
    ((AppDelegate*)[UIApplication sharedApplication].delegate).back = from;
}

+(void) back{
    [ ((AppDelegate*)[UIApplication sharedApplication].delegate).back dismissViewControllerAnimated:NO completion:nil];
}

+(void) setData:(id) data{
    ((AppDelegate*)[UIApplication sharedApplication].delegate).data = data;
}

+(id) getData{
    return ((AppDelegate*)[UIApplication sharedApplication].delegate).data;
}



@end
