//
//  RepositoryModel.m
//  DesafioConcrete
//
//  Created by Renato Mori on 19/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import "RepositoryModel.h"

@implementation RepositoryModel


- (instancetype)initWithRetornoGithub:(RetornoGithub*)retorno
{
    self = [super init];
    if (self && [retorno next]) {
        self.name = [retorno name];
        self.descricao = [retorno description];
        self.login = [retorno login];
        self.avatar_url = [retorno avatar_url];
        self.stargazers_count = [retorno stargazers_count];
        self.forks = [retorno forks];
        self.pulls_url = [retorno pulls_url];
        return self;
    }
    return nil;
}

@end
