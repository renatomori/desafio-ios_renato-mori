//
//  PullRequestModel.m
//  DesafioConcrete
//
//  Created by Renato Mori on 20/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import "PullRequestModel.h"

@implementation PullRequestModel

- (instancetype)initWithRetornoPullRequest:(RetornoPullRequest*)retorno
{
    self = [super init];
    if(self && [retorno next]){
        self.nome = [retorno nome];
        self.fotoUrl = [retorno fotoUrl];
        self.titulo = [retorno titulo];
        self.data = [retorno data];
        self.body = [retorno body];
        self.html_url = [retorno html_url];
        return self;
    }
    return nil;
}
@end
