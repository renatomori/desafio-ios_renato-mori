//
//  PullRequestModel.h
//  DesafioConcrete
//
//  Created by Renato Mori on 20/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RetornoPullRequest.h"

@interface PullRequestModel : NSObject

@property (nonatomic,strong) NSString * nome;
@property (nonatomic,strong) NSString * fotoUrl;
@property (nonatomic,strong) NSString * titulo;
@property (nonatomic,strong) NSString * data;
@property (nonatomic,strong) NSString * body;
@property (nonatomic,strong) NSString * html_url;

- (instancetype)initWithRetornoPullRequest:(RetornoPullRequest*)retorno;


@end
