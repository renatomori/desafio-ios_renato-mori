//
//  RepositoryModel.h
//  DesafioConcrete
//
//  Created by Renato Mori on 19/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RetornoGithub.h"

@interface RepositoryModel : NSObject

@property (nonatomic,strong) NSString * name;
@property (nonatomic,strong) NSString * descricao;
@property (nonatomic,strong) NSString * login;
@property (nonatomic,strong) NSString * avatar_url;

@property (nonatomic) long stargazers_count;
@property (nonatomic) long forks;


@property (nonatomic,strong) NSString * pulls_url;

- (instancetype)initWithRetornoGithub:(RetornoGithub*)retorno;



@end
