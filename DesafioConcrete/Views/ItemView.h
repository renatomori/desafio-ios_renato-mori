//
//  ItemViewCell.h
//  DesafioConcrete
//
//  Created by Renato Mori on 18/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemView : UITableViewCell

@property (nonatomic,weak) IBOutlet UILabel *lblNameRepository;
@property (nonatomic,weak) IBOutlet UILabel *lblDescription;
@property (nonatomic,weak) IBOutlet UILabel *lblBranchs;
@property (nonatomic,weak) IBOutlet UILabel *lblStars;
@property (nonatomic,weak) IBOutlet UIImageView *imgAuthor;

@end
