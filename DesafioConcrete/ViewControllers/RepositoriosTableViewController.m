//
//  RepositoriosTableViewController.m
//  DesafioConcrete
//
//  Created by Renato Mori on 18/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import "RepositoriosTableViewController.h"
#import "ItemViewCell.h"
#import "TrocaTela.h"

#import <SDWebImage/UIImageView+WebCache.h>


@interface RepositoriosTableViewController ()

@end

@implementation RepositoriosTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.data = [[NSMutableArray alloc] init];
    self.page = 0;
    
    [self loadMoreData];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;
}

-(void) loadWs{
    self.page ++;
    self.ws = [[WSGithub alloc]initWithPage:self.page];
    if([self.ws postar]){
        self.retorno = [self.ws getRetorno];
        self.dataTempCount += self.retorno.items.count;
    }

}

-(void) loadMoreData{

    for(int i =0 ; i <10; i++){
        RepositoryModel * item = [[RepositoryModel alloc] initWithRetornoGithub:self.retorno];
        if(item == nil){
            [self loadWs];
            item = [[RepositoryModel alloc] initWithRetornoGithub:self.retorno];
        }
        [self.data addObject:item];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.dataTempCount;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"ItemViewCell";
    NSLog(@"%ld",[indexPath row]);
    //infinite scroll
    while([indexPath row] >= self.data.count-1){
        [self loadMoreData];
        [self.tableView reloadData];
    }

    
    ItemViewCell * cell = (ItemViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ItemViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    RepositoryModel * model = [self.data objectAtIndex:[indexPath row]];
    
    cell.lblNameRepository.text = [model name];
    cell.lblDescription.text = [model descricao];
    cell.lblBranches.text = [NSString stringWithFormat:@"%ld",[model forks]];
    cell.lblStars.text = [NSString stringWithFormat:@"%ld",[model stargazers_count] ];
    
    //NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[model avatar_url]]];
    //cell.imgAuthor.image = [UIImage imageWithData:imageData];
    [cell.imgAuthor sd_setImageWithURL:[NSURL URLWithString:[model avatar_url]]];
    
    cell.url = [model pulls_url ] ;
    


    cell.viewController = self;
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
