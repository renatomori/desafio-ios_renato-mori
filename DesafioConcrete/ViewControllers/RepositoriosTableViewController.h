//
//  RepositoriosTableViewController.h
//  DesafioConcrete
//
//  Created by Renato Mori on 18/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSGithub.h"
#import "RetornoGithub.h"
#import "RepositoryModel.h"

@interface RepositoriosTableViewController : UITableViewController

@property (nonatomic,strong) NSMutableArray * data;
@property (nonatomic) int page;
@property (nonatomic) unsigned long dataTempCount;
@property (nonatomic,strong) WSGithub * ws;
@property (nonatomic,strong) RetornoGithub * retorno;
@end
