//
//  PullsTableViewController.m
//  DesafioConcrete
//
//  Created by Renato Mori on 20/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import "PullsTableViewController.h"
#import "TrocaTela.h"
#import "ItemPullViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PullsTableViewController ()

@end

@implementation PullsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.data = [[NSMutableArray alloc]init];
    [self loadData];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 120;//? tamanho do item height
}

-(void) loadData{
    self.ws = [[WSPullRequest alloc]initWithUrl:(NSString*)[TrocaTela getData]];
    if([self.ws postar]){
        self.retorno = [self.ws getRetorno];
    }
    
    PullRequestModel * item = [[PullRequestModel alloc]initWithRetornoPullRequest:self.retorno];
    
    while(item != nil){
        [self.data addObject:item];
        item = [[PullRequestModel alloc]initWithRetornoPullRequest:self.retorno];
    }
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"ItemPullViewCell";
    NSLog(@"%ld",[indexPath row]);
    
    ItemPullViewCell *cell = (ItemPullViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ItemPullViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    PullRequestModel * model = [self.data objectAtIndex:[indexPath row]];
    
    cell.lblTitulo.text = [model titulo];
    cell.lblBody.text = [model body];
    cell.lblData.text = [model data];
    cell.lblNome.text = [model nome];
    
//    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[model fotoUrl]]];
//    cell.imgFotoUrl.image = [UIImage imageWithData:imageData];
    [cell.imgFotoUrl sd_setImageWithURL:[NSURL URLWithString:[model fotoUrl]]];
    
    cell.html_url = [model html_url];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
