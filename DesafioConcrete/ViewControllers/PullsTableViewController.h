//
//  PullsTableViewController.h
//  DesafioConcrete
//
//  Created by Renato Mori on 20/01/2018.
//  Copyright © 2018 Renato Mori. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSPullRequest.h"
#import "RetornoPullRequest.h"
#import "PullRequestModel.h"

@interface PullsTableViewController : UITableViewController
@property (nonatomic,strong) NSMutableArray * data;


@property (nonatomic,strong) WSPullRequest * ws;
@property (nonatomic,strong) RetornoPullRequest * retorno;



@end
